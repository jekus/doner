grammar Doner;


prog : definition* expr? EOF;

definition : '(' 'define' IDENTIFIER (':' type)? expr ')' ;

expr : let_expr
     | let_seq_expr
     | letrec_expr
     | if_expr
     | logical_expr
     | lambda_expr
     | apply_expr
     | cond_expr
     | var_expr
     | const_expr
     ;

var_expr : IDENTIFIER       # var
         ;

const_expr : CONST_TRUE     # constTrue
           | CONST_FALSE    # constFalse
           | INTEGER        # integer
           ;

let_expr : '(' 'let' '(' bindings ')' expr ')' ;

let_seq_expr : '(' 'let*' '(' bindings ')' expr ')' ;

letrec_expr : '(' 'letrec' '(' bindings ')' expr ')' ;

bindings : ('[' binding ']')+ ;

binding : IDENTIFIER (':' type)? expr ;

type : lambda_type
     | com_type
     | unknown_type
     ;

lambda_type : '(' '->' type+ ')' ;

com_type : IDENTIFIER ;

unknown_type : '?' ;

if_expr : '(' 'if' expr expr expr ')' ;

logical_expr : and_expr
             | or_expr
             ;

and_expr : '(' 'and' expr+ ')' ;

or_expr : '(' 'or' expr+ ')' ;

lambda_expr : '(' 'lambda' '(' formals ')' expr ')' ;

formals : formal* ;

formal : (IDENTIFIER | '[' IDENTIFIER type ']') ;

apply_expr : '(' expr+ ')' ;

cond_expr : '(' 'cond'
                cond_com_branch+
                cond_else_branch?
            ')'
          ;

cond_com_branch : '(' expr expr ')' ;

cond_else_branch : '(' 'else' expr ')' ;

INTEGER : [+\-]? [1-9] DIGIT* 
        | [+\-]? '0'
        ;

IDENTIFIER : VALID_ID_HEADER (LETTER | DIGIT | '_')* ;

fragment
VALID_ID_HEADER : LETTER | [+\-*/=><] | '>=' | '<=' ;

CONST_TRUE : '#t' | '#T' | '#true' | '#True' ;

CONST_FALSE : '#f' | '#F' | '#false' | '#False' ;

fragment
LETTER : [a-zA-Z] ;

fragment
DIGIT : [0-9] ;

COMMENT : ';' .*? NEWLINE -> skip ;

fragment
NEWLINE : '\r'? '\n' ;

WS : [ \f\n\r\t]+ -> skip ;
