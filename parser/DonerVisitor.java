// Generated from Doner.g4 by ANTLR 4.7.1
package doner.Doner.src;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link DonerParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface DonerVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link DonerParser#prog}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProg(DonerParser.ProgContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefinition(DonerParser.DefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(DonerParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code var}
	 * labeled alternative in {@link DonerParser#var_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar(DonerParser.VarContext ctx);
	/**
	 * Visit a parse tree produced by the {@code constTrue}
	 * labeled alternative in {@link DonerParser#const_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstTrue(DonerParser.ConstTrueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code constFalse}
	 * labeled alternative in {@link DonerParser#const_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstFalse(DonerParser.ConstFalseContext ctx);
	/**
	 * Visit a parse tree produced by the {@code integer}
	 * labeled alternative in {@link DonerParser#const_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInteger(DonerParser.IntegerContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#let_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLet_expr(DonerParser.Let_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#let_seq_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLet_seq_expr(DonerParser.Let_seq_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#letrec_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLetrec_expr(DonerParser.Letrec_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#bindings}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBindings(DonerParser.BindingsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#binding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinding(DonerParser.BindingContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(DonerParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#lambda_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLambda_type(DonerParser.Lambda_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#com_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCom_type(DonerParser.Com_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#unknown_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnknown_type(DonerParser.Unknown_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#if_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_expr(DonerParser.If_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#logical_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogical_expr(DonerParser.Logical_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#and_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd_expr(DonerParser.And_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#or_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOr_expr(DonerParser.Or_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#lambda_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLambda_expr(DonerParser.Lambda_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#formals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormals(DonerParser.FormalsContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#formal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFormal(DonerParser.FormalContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#apply_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitApply_expr(DonerParser.Apply_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#cond_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCond_expr(DonerParser.Cond_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#cond_com_branch}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCond_com_branch(DonerParser.Cond_com_branchContext ctx);
	/**
	 * Visit a parse tree produced by {@link DonerParser#cond_else_branch}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCond_else_branch(DonerParser.Cond_else_branchContext ctx);
}