package doner.Doner.src;


final class DonerEvaluator {

	private final DynamicEnv denv;
	private final DStore store;

	DonerEvaluator () {
		denv = new DynamicEnv();
		store = new DStore();
		
		denv.enterNewContext();
		denv.pushVar("+" , new DRef(store , new PrimitiveAdd()));
		denv.pushVar("-" , new DRef(store , new PrimitiveSub()));
		denv.pushVar("*" , new DRef(store , new PrimitiveMul()));
		denv.pushVar("/" , new DRef(store , new PrimitiveDiv()));
		denv.pushVar("=" , new DRef(store , new PrimitiveEqual()));
		denv.pushVar(">" , new DRef(store , new PrimitiveGreater()));
		denv.pushVar("<" , new DRef(store , new PrimitiveLessThan()));
		denv.pushVar(">=" , new DRef(store , new PrimitiveGreaterEqual()));
		denv.pushVar("<=" , new DRef(store , new PrimitiveLessEqual()));
		denv.pushVar("not" , new DRef(store , new PrimitiveNot()));
	}

	Dvalue eval (DonerAST ast) {
		return ast.interp(denv , store);
	}

}