package doner.Doner.src;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


class DonerParseTree2AST extends DonerBaseVisitor<DonerAST> {

	private List<AstExpr> convertExprContexts (List<DonerParser.ExprContext> ectxs) {
		List<AstExpr> exps = new LinkedList<AstExpr>();

		for (DonerParser.ExprContext ectx : ectxs) {
			exps.add((AstExpr)visit(ectx));
		}

		return exps;
	}
	
	private DType convertTypeContext (DonerParser.TypeContext tctx) {
		if (tctx == null || tctx.getChild(0) instanceof DonerParser.Unknown_typeContext) {
			return null;
		}
		else {
			return (DType)visit(tctx);
		}
	}

	private AstLetrecExpr convertDef (DonerParser.ProgContext ctx) {
		List<String> names = new LinkedList<String>();
		List<DType> exps_types = new LinkedList<DType>();
		List<AstExpr> exps = new LinkedList<AstExpr>();
		AstExpr body = (ctx.expr() != null ? (AstExpr)visit(ctx.expr()) : new AstVoid());

		for (DonerParser.DefinitionContext def_ctx : ctx.definition()) {
			names.add(def_ctx.IDENTIFIER().getText());
			exps.add((AstExpr)visit(def_ctx.expr()));
			exps_types.add(convertTypeContext(def_ctx.type()));
		}

		return new AstLetrecExpr(names , exps , exps_types , body);
	}

	@Override
	public DonerAST visitProg (DonerParser.ProgContext ctx) {
		return new AstProg(convertDef(ctx));
	}
	
	@Override
	public DonerAST visitVar (DonerParser.VarContext ctx) {
		return new AstVarExpr(ctx.getText());
	}
	
	@Override
	public DonerAST visitConstTrue (DonerParser.ConstTrueContext ctx) {
		return new AstConstTrue();
	}

	@Override
	public DonerAST visitConstFalse (DonerParser.ConstFalseContext ctx) {
		return new AstConstFalse();
	}
	
	@Override
	public DonerAST visitInteger (DonerParser.IntegerContext ctx) {
		return new AstIntegerExpr(Integer.valueOf(ctx.getText()));
	}
	
	@Override
	public DonerAST visitCom_type (DonerParser.Com_typeContext ctx) {
		String typename = ctx.IDENTIFIER().getText();

		if (typename.equals("int")) {
			return new IntegerType();
		}
		else if (typename.equals("bool")) {
			return new BooleanType();
		}
		else {
			return new DataType(typename);
		}
	}

	@Override
	public DonerAST visitLambda_type (DonerParser.Lambda_typeContext ctx) {
		List<DonerParser.TypeContext> tctxs = ctx.type();
		List<DType> formals_type = new LinkedList<DType>();
		int ts = tctxs.size();
		DType res_type = (DType)visit(tctxs.get(ts - 1));

		for (DonerParser.TypeContext t : tctxs.subList(0 , ts - 1)) {
			formals_type.add((DType)visit(t));
		}

		return new LambdaType(formals_type , res_type);
	}

	@Override
	public DonerAST visitLet_expr (DonerParser.Let_exprContext ctx) {
		List<String> names = new LinkedList<String>();
		List<DType> exps_types = new LinkedList<DType>();
		List<AstExpr> exps = new LinkedList<AstExpr>();
		AstExpr body = (AstExpr)visit(ctx.expr());

		for (DonerParser.BindingContext bctx : ctx.bindings().binding()) {
			names.add(bctx.IDENTIFIER().getText());
			exps.add((AstExpr)visit(bctx.expr()));
			exps_types.add(convertTypeContext(bctx.type()));
		}

		return new AstLetExpr(names , exps , exps_types , body);
	}

	@Override
	public DonerAST visitLet_seq_expr (DonerParser.Let_seq_exprContext ctx) {
		List<String> names = new LinkedList<String>();
		List<DType> exps_types = new LinkedList<DType>();
		List<AstExpr> exps = new LinkedList<AstExpr>();
		AstExpr body = (AstExpr)visit(ctx.expr());

		for (DonerParser.BindingContext bctx : ctx.bindings().binding()) {
			names.add(bctx.IDENTIFIER().getText());
			exps.add((AstExpr)visit(bctx.expr()));
			exps_types.add(convertTypeContext(bctx.type()));
		}

		return new AstLetSeqExpr(names , exps , exps_types , body);
	}

	@Override
	public DonerAST visitLetrec_expr (DonerParser.Letrec_exprContext ctx) {
		List<String> names = new LinkedList<String>();
		List<DType> exps_types = new LinkedList<DType>();
		List<AstExpr> exps = new LinkedList<AstExpr>();
		AstExpr body = (AstExpr)visit(ctx.expr());

		for (DonerParser.BindingContext bctx : ctx.bindings().binding()) {
			names.add(bctx.IDENTIFIER().getText());
			exps.add((AstExpr)visit(bctx.expr()));
			exps_types.add(convertTypeContext(bctx.type()));
		}

		return new AstLetrecExpr(names , exps , exps_types , body);
	}

	@Override
	public DonerAST visitIf_expr (DonerParser.If_exprContext ctx) {
		List<AstExpr> preds = new LinkedList<AstExpr>();
		List<AstExpr> exps = new LinkedList<AstExpr>();

		preds.add((AstExpr)visit(ctx.expr(0)));
		exps.add((AstExpr)visit(ctx.expr(1)));

		preds.add(new AstConstTrue());
		exps.add((AstExpr)visit(ctx.expr(2)));

		return new AstCondExpr(preds , exps);
	}

	@Override
	public DonerAST visitAnd_expr (DonerParser.And_exprContext ctx) {
		List<AstExpr> exps = convertExprContexts(ctx.expr());
		return new AstAndExpr(exps);
	}

	@Override
	public DonerAST visitOr_expr (DonerParser.Or_exprContext ctx) {
		List<AstExpr> exps = convertExprContexts(ctx.expr());
		return new AstOrExpr(exps);
	}

	@Override
	public DonerAST visitLambda_expr (DonerParser.Lambda_exprContext ctx) {
		List<String> formals = new LinkedList<String>();
		List<DType> formals_type = new LinkedList<DType>();
		AstExpr body = (AstExpr)visit(ctx.expr());

		for (DonerParser.FormalContext fctx : ctx.formals().formal()) {
			formals.add(fctx.IDENTIFIER().getText());
			formals_type.add(convertTypeContext(fctx.type()));
		}

		return new AstLambdaExpr(formals , formals_type , body);
	}

	@Override
	public DonerAST visitApply_expr (DonerParser.Apply_exprContext ctx) {
		List<AstExpr> exps = new LinkedList<AstExpr>();

		for (DonerParser.ExprContext ectx : ctx.expr()) {
			exps.add((AstExpr)visit(ectx));
		}

		return new AstApplyExpr(exps);
	}
	
	@Override
	public DonerAST visitCond_expr (DonerParser.Cond_exprContext ctx) {
		List<AstExpr> preds = new LinkedList<AstExpr>();
		List<AstExpr> exps = new LinkedList<AstExpr>();

		for (DonerParser.Cond_com_branchContext com_ctx : ctx.cond_com_branch()) {
			preds.add((AstExpr)visit(com_ctx.expr(0)));
			exps.add((AstExpr)visit(com_ctx.expr(1)));
		}

		if (ctx.cond_else_branch() != null) {
			preds.add(new AstConstTrue());
			exps.add((AstExpr)visit(ctx.cond_else_branch().expr()));

			return new AstCondExpr(preds , exps);
		}
		else {
			return new AstCondExpr(preds , exps);
		}
	}

}