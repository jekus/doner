package doner.Doner.src;

import java.io.Console;
import java.io.StringBufferInputStream;


public class Repl {

	private static boolean isEmptyLine (String line) {
		for (char c : line.toCharArray()) {
			if (!Character.isWhitespace(c)) {
				return false;
			}
		}

		return true;
	}

	public static void main (String[] args) {
		Console cse = System.console();
		String line = cse.readLine("> ");

		while (line != null) {
			if (isEmptyLine(line)) {
				line = cse.readLine("> ");
			}
			else {
				try {
					Dvalue eval_res = DonerInterp.interp(new StringBufferInputStream(line + "\n"));
					String s = eval_res.toString();

					if (!s.isEmpty()) {
						cse.printf("%s%n" , s);
					}
				}
				catch (Exception e) {
					cse.printf("Error - %s%n" , e.getMessage());
				}
				finally {
					line = cse.readLine("> ");
				}
			}
		}

		cse.printf("%n");
	}

}
