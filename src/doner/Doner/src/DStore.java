package doner.Doner.src;

import java.util.List;
import java.util.ArrayList;


class DStore {

	private List<Dvalue> store;

	DStore () {
		store = new ArrayList<Dvalue>();
	}

	int add (Dvalue val) {
		store.add(val);
		return store.size() - 1;
	}

	Dvalue get (int idx) {
		return store.get(idx);
	}

	void set (int idx , Dvalue val) {
		store.set(idx , val);
	}

}

class DRef {

	private DStore store;
	private int idx;

	DRef (DStore store , Dvalue val) {
		this.store = store;
		idx = store.add(val);
	}

	Dvalue get () {
		return store.get(idx);
	}

	void set (Dvalue val) {
		store.set(idx , val);
	}

}
