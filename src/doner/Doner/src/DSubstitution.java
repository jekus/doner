package doner.Doner.src;

import java.util.List;
import java.util.Map;
import java.util.Iterator;
import java.util.HashMap;
import java.util.HashSet;


class DSubstitution {

	private static boolean isEqualLambdaType (DSubstitution subst , LambdaType t1 , LambdaType t2) {
		List<DType> t1_formals_types = t1.getFormalsType();
		List<DType> t2_formals_types = t2.getFormalsType();

		if (t1_formals_types.size() != t2_formals_types.size()) {
			return false;
		}
		else {
			DType t1_res_type = t1.getResultType();
			DType t2_res_type = t2.getResultType();
			Iterator<DType> iter_1 = t1_formals_types.iterator();
			Iterator<DType> iter_2 = t2_formals_types.iterator();

			while (iter_1.hasNext()) {
				DType ta = iter_1.next();
				DType tb = iter_2.next();
				if (!isEqualType(subst , ta , tb)) {
					return false;
				}
			}

			return isEqualType(subst , t1_res_type , t2_res_type);
		}
	}

	private static boolean isEqualType (DSubstitution subst , DType t1 , DType t2) {
		t1 = t1.applySubstitution(subst);
		t2 = t2.applySubstitution(subst);

		if (t1 instanceof TypeVar && t2 instanceof TypeVar) {
			return ((TypeVar)t1).getID() == ((TypeVar)t2).getID();
		}
		else if (t1 instanceof DataType && t2 instanceof DataType) {
			return ((DataType)t1).getName().equals(((DataType)t2).getName());
		}
		else if (t1 instanceof BooleanType && t2 instanceof BooleanType) {
			return true;
		}
		else if (t1 instanceof NumberType && t2 instanceof NumberType) {
			return true;
		}
		else if (t1 instanceof LambdaType && t2 instanceof LambdaType) {
			return isEqualLambdaType(subst , (LambdaType)t1 , (LambdaType)t2);
		}
		else {
			return false;
		}
	}


	private final HashMap<Integer , DType> subst;

	DSubstitution () {
		subst = new HashMap<Integer , DType>();
	}

	public String toString () {
		StringBuilder sb = new StringBuilder();

		for (Map.Entry<Integer , DType> et : subst.entrySet()) {
			int typeid = et.getKey();
			DType t = et.getValue();

			sb.append(String.format("<tvar : %s> = %s%n" , typeid , t));
		}

		return sb.toString();
	}

	void unify (DType t1 , DType t2) {
		if (t1 instanceof TypeVar) {
			setTvar((TypeVar)t1 , t2);
		}
		else if (t2 instanceof TypeVar) {
			setTvar((TypeVar)t2 , t1);
		}
		else if (t1 instanceof LambdaType && t2 instanceof LambdaType) {
			unifyLambdaType((LambdaType)t1 , (LambdaType)t2);
		}
		else if (!isEqualType(this , t1 , t2)) {
			throw new DError(String.format("Can't unify %s and %s" , t1 , t2));
		}
	}

	void unifyTypes (List<DType> tps1 , List<DType> tps2) {
		Iterator<DType> iter_1 = tps1.iterator();
		Iterator<DType> iter_2 = tps2.iterator();

		while (iter_1.hasNext()) {
			unify(iter_1.next() , iter_2.next());
		}
	}

	private void unifyLambdaType (LambdaType t1 , LambdaType t2) {
		List<DType> t1_formals_type = t1.getFormalsType();
		List<DType> t2_formals_type = t2.getFormalsType();
		DType t1_res_type = t1.getResultType();
		DType t2_res_type = t2.getResultType();

		if (t1_formals_type.size() != t2_formals_type.size()) {
			throw new DError(String.format("Can't unify %s and %s" , t1 , t2));
		}
		else {
			unify(t1_res_type , t2_res_type);
			unifyTypes(t1_formals_type , t2_formals_type);
		}
	}

	DType apply (DType t) {
		return t.applySubstitution(this);
	}

	DType get (TypeVar tvar) {
		return subst.getOrDefault(tvar.getID() , tvar);
	}

	private void setTvar (TypeVar tvar , DType t) {
		if (t.haveTvar(tvar)) {
			throw new DError("Having recursive type");
		}
		else {
			/*
			 * Do the substitution first to prevent self-unification when unify
			 * 'tvar_1' with 'Integer' if the 'subst' is [tvar_1 = tvar_1].
			 * 
			 * I don't know whether this situation would happen , but the type
			 * system can handle it.
			 */
			subsWithType(tvar , t);

			int typeid = tvar.getID();
			if (subst.containsKey(typeid)) {
				unify(t , subst.get(typeid));
			}
			else {
				subst.put(tvar.getID() , t);
			}
		}
	}

	private void subsWithType (TypeVar tvar , DType target_t) {
		for (Map.Entry<Integer , DType> et : new HashSet<Map.Entry<Integer , DType>>(subst.entrySet())) {
			subst.put(et.getKey() , et.getValue().subsTvar(tvar , target_t));
		}
	}

}