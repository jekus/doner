package doner.Doner.src;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;


@SuppressWarnings("deprecation")
public class DonerInterp {

	public static DonerAST getAST (String filepath) throws IOException {
		return getAST(new FileInputStream(filepath));
	}

	public static DonerAST getAST (InputStream is) throws IOException {
		ANTLRInputStream input = new ANTLRInputStream(is);
		DonerLexer lexer = new DonerLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		DonerParser parser = new DonerParser(tokens);
		ParseTree tree = parser.prog();
		DonerParseTree2AST conv = new DonerParseTree2AST();

		return conv.visit(tree);
	}

	public static Dvalue interp (String filepath) throws IOException {
		return interp(new FileInputStream(filepath));
	}

	public static Dvalue interp (InputStream is) throws IOException {
		return interp(getAST(is));
	}

	public static Dvalue interp (DonerAST ast) throws IOException {
		DonerEvaluator devaltor = new DonerEvaluator();
		DTypeEngine dte = new DTypeEngine();
		dte.run(ast);
		return devaltor.eval(ast);
	}

	public static void main (String[] args) throws IOException {
		if (args.length > 0) {
			System.out.println(interp(args[0]));
		}
		else {
			System.out.println(interp(System.in));
		}
	}

}
