// Generated from Doner.g4 by ANTLR 4.7.1
package doner.Doner.src;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class DonerLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		INTEGER=18, IDENTIFIER=19, CONST_TRUE=20, CONST_FALSE=21, COMMENT=22, 
		WS=23;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
		"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16", 
		"INTEGER", "IDENTIFIER", "VALID_ID_HEADER", "CONST_TRUE", "CONST_FALSE", 
		"LETTER", "DIGIT", "COMMENT", "NEWLINE", "WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'('", "'define'", "':'", "')'", "'let'", "'let*'", "'letrec'", 
		"'['", "']'", "'->'", "'?'", "'if'", "'and'", "'or'", "'lambda'", "'cond'", 
		"'else'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, "INTEGER", "IDENTIFIER", "CONST_TRUE", 
		"CONST_FALSE", "COMMENT", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public DonerLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Doner.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\31\u00d8\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\4\3\4\3\5\3\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\r\3\r\3\r\3\16\3"+
		"\16\3\16\3\16\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3"+
		"\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\23\5\23|\n\23\3\23\3\23"+
		"\7\23\u0080\n\23\f\23\16\23\u0083\13\23\3\23\5\23\u0086\n\23\3\23\5\23"+
		"\u0089\n\23\3\24\3\24\3\24\3\24\7\24\u008f\n\24\f\24\16\24\u0092\13\24"+
		"\3\25\3\25\3\25\3\25\3\25\3\25\5\25\u009a\n\25\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u00aa\n\26\3\27\3\27"+
		"\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27"+
		"\5\27\u00bc\n\27\3\30\3\30\3\31\3\31\3\32\3\32\7\32\u00c4\n\32\f\32\16"+
		"\32\u00c7\13\32\3\32\3\32\3\32\3\32\3\33\5\33\u00ce\n\33\3\33\3\33\3\34"+
		"\6\34\u00d3\n\34\r\34\16\34\u00d4\3\34\3\34\3\u00c5\2\35\3\3\5\4\7\5\t"+
		"\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23"+
		"%\24\'\25)\2+\26-\27/\2\61\2\63\30\65\2\67\31\3\2\b\4\2--//\3\2\63;\6"+
		"\2,-//\61\61>@\4\2C\\c|\3\2\62;\5\2\13\f\16\17\"\"\2\u00e6\2\3\3\2\2\2"+
		"\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2"+
		"\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2"+
		"\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2"+
		"\2\2\'\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2\63\3\2\2\2\2\67\3\2\2\2\39\3\2"+
		"\2\2\5;\3\2\2\2\7B\3\2\2\2\tD\3\2\2\2\13F\3\2\2\2\rJ\3\2\2\2\17O\3\2\2"+
		"\2\21V\3\2\2\2\23X\3\2\2\2\25Z\3\2\2\2\27]\3\2\2\2\31_\3\2\2\2\33b\3\2"+
		"\2\2\35f\3\2\2\2\37i\3\2\2\2!p\3\2\2\2#u\3\2\2\2%\u0088\3\2\2\2\'\u008a"+
		"\3\2\2\2)\u0099\3\2\2\2+\u00a9\3\2\2\2-\u00bb\3\2\2\2/\u00bd\3\2\2\2\61"+
		"\u00bf\3\2\2\2\63\u00c1\3\2\2\2\65\u00cd\3\2\2\2\67\u00d2\3\2\2\29:\7"+
		"*\2\2:\4\3\2\2\2;<\7f\2\2<=\7g\2\2=>\7h\2\2>?\7k\2\2?@\7p\2\2@A\7g\2\2"+
		"A\6\3\2\2\2BC\7<\2\2C\b\3\2\2\2DE\7+\2\2E\n\3\2\2\2FG\7n\2\2GH\7g\2\2"+
		"HI\7v\2\2I\f\3\2\2\2JK\7n\2\2KL\7g\2\2LM\7v\2\2MN\7,\2\2N\16\3\2\2\2O"+
		"P\7n\2\2PQ\7g\2\2QR\7v\2\2RS\7t\2\2ST\7g\2\2TU\7e\2\2U\20\3\2\2\2VW\7"+
		"]\2\2W\22\3\2\2\2XY\7_\2\2Y\24\3\2\2\2Z[\7/\2\2[\\\7@\2\2\\\26\3\2\2\2"+
		"]^\7A\2\2^\30\3\2\2\2_`\7k\2\2`a\7h\2\2a\32\3\2\2\2bc\7c\2\2cd\7p\2\2"+
		"de\7f\2\2e\34\3\2\2\2fg\7q\2\2gh\7t\2\2h\36\3\2\2\2ij\7n\2\2jk\7c\2\2"+
		"kl\7o\2\2lm\7d\2\2mn\7f\2\2no\7c\2\2o \3\2\2\2pq\7e\2\2qr\7q\2\2rs\7p"+
		"\2\2st\7f\2\2t\"\3\2\2\2uv\7g\2\2vw\7n\2\2wx\7u\2\2xy\7g\2\2y$\3\2\2\2"+
		"z|\t\2\2\2{z\3\2\2\2{|\3\2\2\2|}\3\2\2\2}\u0081\t\3\2\2~\u0080\5\61\31"+
		"\2\177~\3\2\2\2\u0080\u0083\3\2\2\2\u0081\177\3\2\2\2\u0081\u0082\3\2"+
		"\2\2\u0082\u0089\3\2\2\2\u0083\u0081\3\2\2\2\u0084\u0086\t\2\2\2\u0085"+
		"\u0084\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u0087\3\2\2\2\u0087\u0089\7\62"+
		"\2\2\u0088{\3\2\2\2\u0088\u0085\3\2\2\2\u0089&\3\2\2\2\u008a\u0090\5)"+
		"\25\2\u008b\u008f\5/\30\2\u008c\u008f\5\61\31\2\u008d\u008f\7a\2\2\u008e"+
		"\u008b\3\2\2\2\u008e\u008c\3\2\2\2\u008e\u008d\3\2\2\2\u008f\u0092\3\2"+
		"\2\2\u0090\u008e\3\2\2\2\u0090\u0091\3\2\2\2\u0091(\3\2\2\2\u0092\u0090"+
		"\3\2\2\2\u0093\u009a\5/\30\2\u0094\u009a\t\4\2\2\u0095\u0096\7@\2\2\u0096"+
		"\u009a\7?\2\2\u0097\u0098\7>\2\2\u0098\u009a\7?\2\2\u0099\u0093\3\2\2"+
		"\2\u0099\u0094\3\2\2\2\u0099\u0095\3\2\2\2\u0099\u0097\3\2\2\2\u009a*"+
		"\3\2\2\2\u009b\u009c\7%\2\2\u009c\u00aa\7v\2\2\u009d\u009e\7%\2\2\u009e"+
		"\u00aa\7V\2\2\u009f\u00a0\7%\2\2\u00a0\u00a1\7v\2\2\u00a1\u00a2\7t\2\2"+
		"\u00a2\u00a3\7w\2\2\u00a3\u00aa\7g\2\2\u00a4\u00a5\7%\2\2\u00a5\u00a6"+
		"\7V\2\2\u00a6\u00a7\7t\2\2\u00a7\u00a8\7w\2\2\u00a8\u00aa\7g\2\2\u00a9"+
		"\u009b\3\2\2\2\u00a9\u009d\3\2\2\2\u00a9\u009f\3\2\2\2\u00a9\u00a4\3\2"+
		"\2\2\u00aa,\3\2\2\2\u00ab\u00ac\7%\2\2\u00ac\u00bc\7h\2\2\u00ad\u00ae"+
		"\7%\2\2\u00ae\u00bc\7H\2\2\u00af\u00b0\7%\2\2\u00b0\u00b1\7h\2\2\u00b1"+
		"\u00b2\7c\2\2\u00b2\u00b3\7n\2\2\u00b3\u00b4\7u\2\2\u00b4\u00bc\7g\2\2"+
		"\u00b5\u00b6\7%\2\2\u00b6\u00b7\7H\2\2\u00b7\u00b8\7c\2\2\u00b8\u00b9"+
		"\7n\2\2\u00b9\u00ba\7u\2\2\u00ba\u00bc\7g\2\2\u00bb\u00ab\3\2\2\2\u00bb"+
		"\u00ad\3\2\2\2\u00bb\u00af\3\2\2\2\u00bb\u00b5\3\2\2\2\u00bc.\3\2\2\2"+
		"\u00bd\u00be\t\5\2\2\u00be\60\3\2\2\2\u00bf\u00c0\t\6\2\2\u00c0\62\3\2"+
		"\2\2\u00c1\u00c5\7=\2\2\u00c2\u00c4\13\2\2\2\u00c3\u00c2\3\2\2\2\u00c4"+
		"\u00c7\3\2\2\2\u00c5\u00c6\3\2\2\2\u00c5\u00c3\3\2\2\2\u00c6\u00c8\3\2"+
		"\2\2\u00c7\u00c5\3\2\2\2\u00c8\u00c9\5\65\33\2\u00c9\u00ca\3\2\2\2\u00ca"+
		"\u00cb\b\32\2\2\u00cb\64\3\2\2\2\u00cc\u00ce\7\17\2\2\u00cd\u00cc\3\2"+
		"\2\2\u00cd\u00ce\3\2\2\2\u00ce\u00cf\3\2\2\2\u00cf\u00d0\7\f\2\2\u00d0"+
		"\66\3\2\2\2\u00d1\u00d3\t\7\2\2\u00d2\u00d1\3\2\2\2\u00d3\u00d4\3\2\2"+
		"\2\u00d4\u00d2\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5\u00d6\3\2\2\2\u00d6\u00d7"+
		"\b\34\2\2\u00d78\3\2\2\2\17\2{\u0081\u0085\u0088\u008e\u0090\u0099\u00a9"+
		"\u00bb\u00c5\u00cd\u00d4\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}