package doner.Doner.src;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


abstract class DonerAST {

	abstract Dvalue interp (DynamicEnv denv , DStore store);
	
	abstract DType getType (StaticEnv senv , DSubstitution subst);
	
	protected static final List<DType> typeOfExps (List<AstExpr> exps , StaticEnv senv , DSubstitution subst) {
		List<DType> types = new LinkedList<DType>();

		for (AstExpr exp : exps) {
			types.add(exp.getType(senv , subst));
		}

		return types;
	}
	
	protected static final List<DType> convertUnknownTypes (List<DType> old_types , DSubstitution subst) {
		List<DType> new_types = new LinkedList<DType>();

		for (DType t : old_types) {
			if (t == null) {
				new_types.add(new TypeVar(subst));
			}
			else {
				new_types.add(t);
			}
		}

		return new_types;
	}

}

final class AstProg extends DonerAST {

	private final AstExpr exp;

	AstProg (AstExpr e) {
		exp = e;
	}

	AstExpr getExpr () {
		return exp;
	}

	@Override
	Dvalue interp (DynamicEnv denv , DStore store) {
		return exp.interp(denv , store);
	}

	@Override
	DType getType (StaticEnv senv , DSubstitution subst) {
		return exp.getType(senv , subst);
	}

}


abstract class AstExpr extends DonerAST {
}

final class AstVoid extends AstExpr {

	@Override
	Dvalue interp (DynamicEnv denv , DStore store) {
		return new DVoid();
	}

	@Override
	DType getType (StaticEnv senv , DSubstitution subst) {
		return new VoidType();
	}

}

final class AstLetExpr extends AstExpr {

	private final List<String> names;
	private final List<AstExpr> exps;
	private final List<DType> exps_types;
	private final AstExpr body;

	AstLetExpr (List<String> names , List<AstExpr> exps , List<DType> exps_types , AstExpr body) {
		this.names = names;
		this.exps = exps;
		this.exps_types = exps_types;
		this.body = body;
	}

	List<String> getNames () {
		return names;
	}

	List<AstExpr> getExps () {
		return exps;
	}

	List<DType> getExpsTypes () {
		return exps_types;
	}

	AstExpr getBody () {
		return body;
	}

	private static DynamicEnv buildLetDEnv (
			List<String> names , List<AstExpr> exps ,
			DynamicEnv old_denv , DStore store)
	{
		DynamicEnv new_denv = old_denv.clone();
		new_denv.enterNewContext();

		
		Iterator<String> name_iter = names.iterator();
		Iterator<AstExpr> exp_iter = exps.iterator();

		while (name_iter.hasNext()) {
			new_denv.pushVar(name_iter.next() , new DRef(store , exp_iter.next().interp(old_denv , store)));
		}

		return new_denv;
	}
	
	private static StaticEnv buildLetSEnv (List<String> names , List<DType> types , StaticEnv old_senv) {
		StaticEnv new_senv = old_senv.clone();
		new_senv.addNewContext();

		
		Iterator<String> name_iter = names.iterator();
		Iterator<DType> type_iter = types.iterator();

		while (name_iter.hasNext()) {
			new_senv.pushVar(name_iter.next() , type_iter.next());
		}

		return new_senv;
	}

	@Override
	Dvalue interp (DynamicEnv denv , DStore store) {
		DynamicEnv bodyenv = buildLetDEnv(names , exps , denv , store);
		return body.interp(bodyenv , store);
	}

	@Override
	DType getType (StaticEnv senv , DSubstitution subst) {
		List<DType> expect_exps_types = convertUnknownTypes(exps_types , subst);
		List<DType> infer_exps_types = typeOfExps(exps , senv , subst);
		StaticEnv new_senv = buildLetSEnv(names , infer_exps_types , senv);
		
		subst.unifyTypes(expect_exps_types , infer_exps_types);
		
		return body.getType(new_senv , subst);
	}

}

final class AstLetSeqExpr extends AstExpr {

	private final List<String> names;
	private final List<AstExpr> exps;
	private final List<DType> exps_types;
	private final AstExpr body;

	AstLetSeqExpr (List<String> names , List<AstExpr> exps , List<DType> exps_types , AstExpr body) {
		this.names = names;
		this.exps = exps;
		this.exps_types = exps_types;
		this.body = body;
	}

	List<String> getNames () {
		return names;
	}

	List<AstExpr> getExps () {
		return exps;
	}

	List<DType> getExpsTypes () {
		return exps_types;
	}

	AstExpr getBody () {
		return body;
	}

	private static DynamicEnv buildLetseqDEnv (
			List<String> names , List<AstExpr> exps ,
			DynamicEnv old_denv , DStore store)
	{
		DynamicEnv new_denv = old_denv.clone();
		new_denv.enterNewContext();

		
		Iterator<String> name_iter = names.iterator();
		Iterator<AstExpr> expr_iter = exps.iterator();

		while (name_iter.hasNext()) {
			new_denv.pushVar(name_iter.next() , new DRef(store , expr_iter.next().interp(new_denv , store)));
		}

		return new_denv;
	}
	
	private static StaticEnv buildLetseqSEnv (
			List<String> names , List<AstExpr> exps , List<DType> expect_exps_types ,
			StaticEnv old_senv , DSubstitution subst)
	{
		StaticEnv new_senv = old_senv.clone();
		new_senv.addNewContext();

		
		Iterator<String> names_iter = names.iterator();
		Iterator<AstExpr> exps_iter = exps.iterator();
		Iterator<DType> expect_exps_types_iter = expect_exps_types.iterator();

		while (names_iter.hasNext()) {
			String name = names_iter.next();
			DType expect_type = expect_exps_types_iter.next();
			DType infer_type = exps_iter.next().getType(new_senv , subst);

			subst.unify(infer_type , expect_type);
			new_senv.pushVar(name , infer_type);
		}

		return new_senv;
	}

	@Override
	Dvalue interp (DynamicEnv denv , DStore store) {
		DynamicEnv bodyenv = buildLetseqDEnv(names , exps , denv , store);
		return body.interp(bodyenv , store);
	}

	@Override
	DType getType (StaticEnv senv , DSubstitution subst) {
		List<DType> expect_exps_types = convertUnknownTypes(exps_types , subst);
		StaticEnv new_senv = buildLetseqSEnv(names , exps , expect_exps_types , senv , subst);
		
		return body.getType(new_senv , subst);
	}

}

final class AstLetrecExpr extends AstExpr {

	private final List<String> names;
	private final List<AstExpr> exps;
	private final List<DType> exps_types;
	private final AstExpr body;

	AstLetrecExpr (List<String> names , List<AstExpr> exps , List<DType> exps_types , AstExpr body) {
		this.names = names;
		this.exps = exps;
		this.exps_types = exps_types;
		this.body = body;
	}

	List<String> getNames () {
		return names;
	}

	List<AstExpr> getExps () {
		return exps;
	}

	List<DType> getExpsTypes () {
		return exps_types;
	}

	AstExpr getBody () {
		return body;
	}

	private static DynamicEnv buildLetrecDEnv (
			List<String> names , List<AstExpr> exps ,
			DynamicEnv old_denv , DStore store)
	{
		DynamicEnv new_denv = old_denv.clone();
		new_denv.enterNewContext();

		
		Iterator<String> name_iter = names.iterator();
		Iterator<AstExpr> expr_iter = exps.iterator();
		DynamicEnvPtr env_ptr = new DynamicEnvPtr();

		while (name_iter.hasNext()) {
			String name = name_iter.next();
			AstExpr exp = expr_iter.next();

			if (exp instanceof AstLambdaExpr) {
				AstLambdaExpr lbexp = (AstLambdaExpr)exp;
				DLambda lval = new DLambda(lbexp.getFormals() , lbexp.getBody() , env_ptr);
				new_denv.pushVar(name , new DRef(store , lval));
			}
			else {
				new_denv.pushVar(name , new DRef(store , exp.interp(new_denv , store)));
			}
		}

		env_ptr.setEnv(new_denv);

		return new_denv;
	}
	
	private static StaticEnv buildLetrecSEnv (
			List<String> names , List<AstExpr> exps , List<DType> expect_exps_types ,
			StaticEnv old_senv , DSubstitution subst)
	{
		StaticEnv new_senv = old_senv.clone();
		new_senv.addNewContext();

		List<TypeVar> tvars = new LinkedList<TypeVar>();
		for (String name : names) {
			TypeVar tvar = new TypeVar(subst);
			new_senv.pushVar(name , tvar);
			tvars.add(tvar);
		}
		

		Iterator<TypeVar> tvars_iter = tvars.iterator();
		Iterator<AstExpr> exps_iter = exps.iterator();
		Iterator<DType> expect_exps_types_iter = expect_exps_types.iterator();

		while (tvars_iter.hasNext()) {
			TypeVar tvar = tvars_iter.next();
			DType expect_type = expect_exps_types_iter.next();
			DType infer_type = exps_iter.next().getType(new_senv , subst);

			subst.unify(tvar , expect_type);
			subst.unify(infer_type , expect_type);
		}

		return new_senv;
	}

	@Override
	Dvalue interp (DynamicEnv denv , DStore store) {
		DynamicEnv bodyenv = buildLetrecDEnv(names , exps , denv , store);
		return body.interp(bodyenv , store);
	}

	@Override
	DType getType (StaticEnv senv , DSubstitution subst) {
		List<DType> expect_exps_types = convertUnknownTypes(exps_types , subst);
		StaticEnv new_senv = buildLetrecSEnv(names , exps , expect_exps_types , senv , subst);
		
		return body.getType(new_senv , subst);
	}

}

final class AstLambdaExpr extends AstExpr {

	private final List<String> formals;
	private final List<DType> formals_types;
	private final AstExpr body;

	AstLambdaExpr (List<String> fs , List<DType> fst , AstExpr bd) {
		formals = fs;
		formals_types = fst;
		body = bd;
	}

	List<String> getFormals () {
		return formals;
	}

	List<DType> getFormalsTypes () {
		return formals_types;
	}

	AstExpr getBody () {
		return body;
	}
	
	private static StaticEnv buildLambdaSEnv (List<String> names , List<DType> types , StaticEnv old_senv) {
		StaticEnv new_senv = old_senv.clone();
		new_senv.addNewContext();

		
		Iterator<String> name_iter = names.iterator();
		Iterator<DType> type_iter = types.iterator();

		while (name_iter.hasNext()) {
			new_senv.pushVar(name_iter.next() , type_iter.next());
		}

		return new_senv;
	}

	@Override
	Dvalue interp (DynamicEnv denv , DStore store) {
		return new DLambda(formals , body , denv.clone());
	}

	@Override
	DType getType (StaticEnv senv , DSubstitution subst) {
		List<DType> expect_formals_types = convertUnknownTypes(formals_types , subst);
		StaticEnv new_senv = buildLambdaSEnv(formals , expect_formals_types , senv);
		DType res_type = body.getType(new_senv , subst);

		return new LambdaType(expect_formals_types , res_type);
	}

}

final class AstApplyExpr extends AstExpr {

	private final List<AstExpr> exps;

	AstApplyExpr (List<AstExpr> exps) {
		this.exps = exps;
	}

	List<AstExpr> getExprs () {
		return exps;
	}
	
	private static DynamicEnv buildApplyDEnv (
			List<String> names , List<AstExpr> exps ,
			DynamicEnv cur_denv , DynamicEnv proc_saved_denv , DStore store)
	{
		DynamicEnv new_denv = proc_saved_denv.clone();
		new_denv.enterNewContext();

		
		Iterator<String> name_iter = names.iterator();
		Iterator<AstExpr> expr_iter = exps.iterator();

		while (name_iter.hasNext()) {
			DRef ref = new DRef(store , expr_iter.next().interp(cur_denv , store));
			new_denv.pushVar(name_iter.next() , ref);
		}

		return new_denv;
	}

	@Override
	Dvalue interp (DynamicEnv denv , DStore store) {
		Dvalue val = exps.get(0).interp(denv , store);

		if (val instanceof PrimitiveProc) {
			PrimitiveProc proc = (PrimitiveProc)val;
			return proc.apply(exps.subList(1 , exps.size()) , denv , store);
		}
		else {
			DLambda proc = (DLambda)val;
			List<String> formals_names = proc.getFormals();
			List<AstExpr> args = exps.subList(1 , exps.size());
			DynamicEnv proc_saved_denv = proc.getEnv();
			DynamicEnv proc_body_env = buildApplyDEnv(formals_names , args , denv , proc_saved_denv , store);

			return proc.getBody().interp(proc_body_env , store);
		}
	}

	@Override
	DType getType (StaticEnv senv , DSubstitution subst) {
		List<DType> exps_types = typeOfExps(exps , senv , subst);
		List<DType> expect_formals_types = exps_types.subList(1 , exps_types.size());
		TypeVar expect_res_type = new TypeVar(subst);
		DType actual_proc_t = exps_types.get(0);
		LambdaType expect_proc_t = new LambdaType(expect_formals_types , expect_res_type);

		if (actual_proc_t instanceof PrimitiveProcType) {
			((PrimitiveProcType)actual_proc_t).unifyWith(expect_proc_t , subst);
		}
		else {
			subst.unify(actual_proc_t , expect_proc_t);
		}

		return expect_res_type;
	}

}

final class AstCondExpr extends AstExpr {

	private final List<AstExpr> preds;
	private final List<AstExpr> exps;

	AstCondExpr (List<AstExpr> preds , List<AstExpr> exps) {
		this.preds = preds;
		this.exps = exps;
	}

	@Override
	Dvalue interp (DynamicEnv denv , DStore store) {
		Iterator<AstExpr> preds_iter = preds.iterator();
		Iterator<AstExpr> exps_iter = exps.iterator();

		while (preds_iter.hasNext()) {
			AstExpr pred = preds_iter.next();
			AstExpr exp = exps_iter.next();
			boolean pred_res = ((DBoolean)pred.interp(denv , store)).getRep();

			if (pred_res == true) {
				return exp.interp(denv , store);
			}
		}

		return new DVoid();
	}

	@Override
	DType getType (StaticEnv senv , DSubstitution subst) {
		TypeVar ret_type = new TypeVar(subst);
		BooleanType bool_type = new BooleanType();
		Iterator<AstExpr> preds_iter = preds.iterator();
		Iterator<AstExpr> exps_iter = exps.iterator();

		while (preds_iter.hasNext()) {
			AstExpr pred = preds_iter.next();
			AstExpr exp = exps_iter.next();

			subst.unify(bool_type , pred.getType(senv , subst));
			subst.unify(ret_type , exp.getType(senv , subst));
		}
		
		return ret_type;
	}

}

final class AstVarExpr extends AstExpr {

	private final String name;

	AstVarExpr (String name) {
		this.name = name;
	}

	String getName () {
		return name;
	}

	@Override
	Dvalue interp (DynamicEnv denv , DStore store) {
		return denv.getVar(name).get();
	}

	@Override
	DType getType (StaticEnv senv , DSubstitution subst) {
		return senv.getVar(name);
	}

}

final class AstIntegerExpr extends AstExpr {

	private final int val;

	AstIntegerExpr (int val) {
		this.val = val;
	}

	int getVal () {
		return val;
	}

	@Override
	Dvalue interp (DynamicEnv denv , DStore store) {
		return new DInteger(val);
	}

	@Override
	DType getType (StaticEnv senv , DSubstitution subst) {
		return new IntegerType();
	}

}

final class AstConstTrue extends AstExpr {
	
	@Override
	Dvalue interp (DynamicEnv denv , DStore store) {
		return new DBoolean(true);
	}

	@Override
	DType getType (StaticEnv senv , DSubstitution subst) {
		return new BooleanType();
	}
	
}

final class AstConstFalse extends AstExpr {
	
	@Override
	Dvalue interp (DynamicEnv denv , DStore store) {
		return new DBoolean(false);
	}

	@Override
	DType getType (StaticEnv senv , DSubstitution subst) {
		return new BooleanType();
	}
	
}

abstract class AstLogicalExpr extends AstExpr {

	protected List<AstExpr> exps;

	AstLogicalExpr (List<AstExpr> exps) {
		this.exps = exps;
	}

	List<AstExpr> getExprs () {
		return exps;
	}

}

final class AstAndExpr extends AstLogicalExpr {

	AstAndExpr (List<AstExpr> exps) {
		super(exps);
	}

	@Override
	Dvalue interp (DynamicEnv denv , DStore store) {
		for (AstExpr exp : exps) {
			boolean val = ((DBoolean)exp.interp(denv , store)).getRep();
			
			if (val == false) {
				return new DBoolean(false);
			}
			else {
				continue;
			}
		}

		return new DBoolean(true);
	}

	@Override
	DType getType (StaticEnv senv , DSubstitution subst) {
		DType bool_t = new BooleanType();
		List<DType> exps_types = typeOfExps(exps , senv , subst);

		for (DType t : exps_types) {
			subst.unify(t , bool_t);
		}

		return bool_t;
	}

}

final class AstOrExpr extends AstLogicalExpr {

	AstOrExpr (List<AstExpr> exps) {
		super(exps);
	}

	@Override
	Dvalue interp (DynamicEnv denv , DStore store) {
		for (AstExpr exp : exps) {
			boolean val = ((DBoolean)exp.interp(denv , store)).getRep();
			
			if (val == true) {
				return new DBoolean(true);
			}
			else {
				continue;
			}
		}

		return new DBoolean(false);
	}

	@Override
	DType getType (StaticEnv senv , DSubstitution subst) {
		DType bool_t = new BooleanType();
		List<DType> exps_types = typeOfExps(exps , senv , subst);

		for (DType t : exps_types) {
			subst.unify(t , bool_t);
		}

		return bool_t;
	}

}