package doner.Doner.src;

import java.util.ArrayList;
import java.util.List;


abstract class Dvalue {

}

abstract class PrimitiveProc extends Dvalue {
	
	abstract Dvalue apply (List<AstExpr> exps , DynamicEnv denv , DStore store);

}

class PrimitiveAdd extends PrimitiveProc {

	@Override
	public String toString () {
		return "#<procedure : +>";
	}

	Dvalue apply (List<AstExpr> exps , DynamicEnv denv , DStore store) {
		DNumber res = new DInteger(0);

		for (AstExpr exp : exps) {
			DNumber val = (DNumber)exp.interp(denv , store);
			res = DNumber.add(res , val);
		}

		return res;
	}

}

class PrimitiveSub extends PrimitiveProc {

	@Override
	public String toString () {
		return "#<procedure : ->";
	}

	Dvalue apply (List<AstExpr> exps , DynamicEnv denv , DStore store) {
		DNumber res = (DNumber)exps.get(0).interp(denv , store);

		if (exps.size() == 1) {
			return res.negate();
		}
		else {
			for (AstExpr exp : exps.subList(1 , exps.size())) {
				DNumber val = (DNumber)exp.interp(denv , store);
				res = DNumber.sub(res , val);
			}

			return res;
		}
	}

}

class PrimitiveMul extends PrimitiveProc {

	@Override
	public String toString () {
		return "#<procedure : *>";
	}

	Dvalue apply (List<AstExpr> exps , DynamicEnv denv , DStore store) {
		DNumber res = new DInteger(1);

		for (AstExpr exp : exps) {
			DNumber val = (DNumber)exp.interp(denv , store);
			res = DNumber.mul(res , val);
		}

		return res;
	}

}

class PrimitiveDiv extends PrimitiveProc {

	@Override
	public String toString () {
		return "#<procedure : />";
	}

	Dvalue apply (List<AstExpr> exps , DynamicEnv denv , DStore store) {
		DNumber first_num = (DNumber)exps.get(0).interp(denv , store);

		if (exps.size() == 1) {
			return new DFraction(new DInteger(1) , first_num);
		}
		else {
			DNumber numerator = first_num;
			DNumber denominator = new DInteger(1);

			for (AstExpr exp : exps.subList(1 , exps.size())) {
				DNumber val = (DNumber)exp.interp(denv , store);
				denominator = DNumber.mul(denominator , val);
			}

			return new DFraction(numerator , denominator);
		}
	}

}

class PrimitiveEqual extends PrimitiveProc {

	@Override
	public String toString () {
		return "#<procedure : =>";
	}

	Dvalue apply (List<AstExpr> exps , DynamicEnv denv , DStore store) {
		DNumber old_num = (DNumber)exps.get(0).interp(denv , store);

		for (AstExpr exp : exps.subList(1 , exps.size())) {
			DNumber new_num = (DNumber)exp.interp(denv , store);

			if (DNumber.compare(old_num , new_num) != 0) {
				return new DBoolean(false);
			}
			else {
				old_num = new_num;
			}
		}

		return new DBoolean(true);
	}

}

class PrimitiveGreater extends PrimitiveProc {

	@Override
	public String toString () {
		return "#<procedure : >>";
	}

	Dvalue apply (List<AstExpr> exps , DynamicEnv denv , DStore store) {
		DNumber old_num = (DNumber)exps.get(0).interp(denv , store);

		for (AstExpr exp : exps.subList(1 , exps.size())) {
			DNumber new_num = (DNumber)exp.interp(denv , store);

			if (DNumber.compare(old_num , new_num) <= 0) {
				return new DBoolean(false);
			}
			else {
				old_num = new_num;
			}
		}

		return new DBoolean(true);
	}

}

class PrimitiveLessThan extends PrimitiveProc {

	@Override
	public String toString () {
		return "#<procedure : <>";
	}

	Dvalue apply (List<AstExpr> exps , DynamicEnv denv , DStore store) {
		DNumber old_num = (DNumber)exps.get(0).interp(denv , store);

		for (AstExpr exp : exps.subList(1 , exps.size())) {
			DNumber new_num = (DNumber)exp.interp(denv , store);

			if (DNumber.compare(old_num , new_num) >= 0) {
				return new DBoolean(false);
			}
			else {
				old_num = new_num;
			}
		}

		return new DBoolean(true);
	}

}

class PrimitiveGreaterEqual extends PrimitiveProc {

	@Override
	public String toString () {
		return "#<procedure : >=>";
	}

	Dvalue apply (List<AstExpr> exps , DynamicEnv denv , DStore store) {
		DNumber old_num = (DNumber)exps.get(0).interp(denv , store);

		for (AstExpr exp : exps.subList(1 , exps.size())) {
			DNumber new_num = (DNumber)exp.interp(denv , store);

			if (DNumber.compare(old_num , new_num) < 0) {
				return new DBoolean(false);
			}
			else {
				old_num = new_num;
			}
		}

		return new DBoolean(true);
	}

}

class PrimitiveLessEqual extends PrimitiveProc {

	@Override
	public String toString () {
		return "#<procedure : <=>";
	}

	Dvalue apply (List<AstExpr> exps , DynamicEnv denv , DStore store) {
		DNumber old_num = (DNumber)exps.get(0).interp(denv , store);

		for (AstExpr exp : exps.subList(1 , exps.size())) {
			DNumber new_num = (DNumber)exp.interp(denv , store);

			if (DNumber.compare(old_num , new_num) > 0) {
				return new DBoolean(false);
			}
			else {
				old_num = new_num;
			}
		}

		return new DBoolean(true);
	}

}

class PrimitiveNot extends PrimitiveProc {

	@Override
	public String toString () {
		return "#<procedure : not>";
	}

	Dvalue apply (List<AstExpr> exps , DynamicEnv denv , DStore store) {
		boolean val = ((DBoolean)exps.get(0).interp(denv , store)).getRep();
		return new DBoolean(!val);
	}

}

class DVoid extends Dvalue {

	@Override
	public String toString () {
		return "";
	}

}

class DBoolean extends Dvalue {

	private final boolean val;

	DBoolean (boolean val) {
		this.val = val;
	}

	boolean getRep () {
		return val;
	}

	@Override
	public String toString () {
		return Boolean.toString(val);
	}

}

abstract class DNumber extends Dvalue {
	
	/*
	 * Return 0 if 'n1' == 'n2' , -1 if 'n1' < 'n2' , 1 if 'n1' > 'n2'
	 */
	static int compare (DNumber n1 , DNumber n2) {
		if (n1 instanceof DInteger && n2 instanceof DInteger) {
			return compare((DInteger)n1 , (DInteger)n2);
		}
		else if (n1 instanceof DFraction && n2 instanceof DFraction) {
			return compare((DFraction)n1 , (DFraction)n2);
		}
		else if (n1 instanceof DFraction && n2 instanceof DInteger) {
			return compare((DFraction)n1 , (DInteger)n2);
		}
		else if (n1 instanceof DInteger && n2 instanceof DFraction) {
			return compare((DInteger)n1 , (DFraction)n2);
		}
		else {
			throw new DError("Impossible branch");
		}
	}
	
	private static int compare (DInteger i1 , DInteger i2) {
		long v1 = i1.getRep();
		long v2 = i2.getRep();
		
		return convertCmpRes(v1 - v2);
	}
	
	private static int compare (DFraction f1 , DFraction f2) {
		long f1_n = f1.getNumerator();
		long f1_d = f1.getDenominator();
		long f2_n = f2.getNumerator();
		long f2_d = f2.getDenominator();
		
		return convertCmpRes(f1_n * f2_d - f2_n * f1_d);
	}
	
	private static int compare (DFraction f , DInteger i) {
		long fn = f.getNumerator();
		long fd = f.getDenominator();

		return convertCmpRes(fn - i.getRep() * fd);
	}
	
	private static int compare (DInteger i , DFraction f) {
		return -compare(f , i);
	}
	
	private static int convertCmpRes (long res) {
		if (res < 0) {
			return -1;
		}
		else if (res == 0) {
			return 0;
		}
		else {
			return 1;
		}
	}

	static DNumber add (DNumber n1 , DNumber n2) {
		if (n1 instanceof DInteger && n2 instanceof DInteger) {
			return DNumber.add((DInteger)n1 , (DInteger)n2);
		}
		else if (n1 instanceof DFraction && n2 instanceof DFraction) {
			return DNumber.add((DFraction)n1 , (DFraction)n2);
		}
		else if (n1 instanceof DFraction && n2 instanceof DInteger) {
			return DNumber.add((DFraction)n1 , (DInteger)n2);
		}
		else if (n1 instanceof DInteger && n2 instanceof DFraction) {
			return DNumber.add((DFraction)n2 , (DInteger)n1);
		}
		else {
			throw new DError("Impossible branch");
		}
	}

	static DNumber sub (DNumber n1 , DNumber n2) {
		return DNumber.add(n1 , n2.negate());
	}

	static DNumber mul (DNumber n1 , DNumber n2) {
		if (n1 instanceof DInteger && n2 instanceof DInteger) {
			return DNumber.mul((DInteger)n1 , (DInteger)n2);
		}
		else if (n1 instanceof DFraction && n2 instanceof DFraction) {
			return DNumber.mul((DFraction)n1 , (DFraction)n2);
		}
		else if (n1 instanceof DFraction && n2 instanceof DInteger) {
			return DNumber.mul((DFraction)n1 , (DInteger)n2);
		}
		else if (n1 instanceof DInteger && n2 instanceof DFraction) {
			return DNumber.mul((DFraction)n2 , (DInteger)n1);
		}
		else {
			throw new DError("Impossible branch");
		}
	}

	static DNumber div (DNumber n1 , DNumber n2) {
		if (n1 instanceof DInteger && n2 instanceof DInteger) {
			return DNumber.div((DInteger)n1 , (DInteger)n2);
		}
		else if (n1 instanceof DFraction && n2 instanceof DFraction) {
			return DNumber.div((DFraction)n1 , (DFraction)n2);
		}
		else if (n1 instanceof DFraction && n2 instanceof DInteger) {
			return DNumber.div((DFraction)n1 , (DInteger)n2);
		}
		else if (n1 instanceof DInteger && n2 instanceof DFraction) {
			return DNumber.div((DFraction)n2 , (DInteger)n1);
		}
		else {
			throw new DError("Impossible branch");
		}
	}
	
	private static DInteger add (DInteger i1 , DInteger i2) {
		long v1 = i1.getRep();
		long v2 = i2.getRep();

		return new DInteger(v1 + v2);
	}

	private static DFraction add (DFraction f , DInteger i) {
		long numerator = f.getNumerator() + i.getRep() * f.getDenominator();
		long denominator = f.getDenominator();

		return new DFraction(numerator , denominator);
	}

	private static DFraction add (DFraction fa , DFraction fb) {
		long fa_nrator = fa.getNumerator();
		long fb_nrator = fb.getNumerator();
		long fa_dtor = fa.getDenominator();
		long fb_dtor = fb.getDenominator();

		long numerator = fa_nrator * fb_dtor + fb_nrator * fa_dtor;
		long denominator = fa_dtor * fb_dtor;

		return new DFraction(numerator , denominator);
	}
	
	private static DInteger mul (DInteger i1 , DInteger i2) {
		long v1 = i1.getRep();
		long v2 = i2.getRep();

		return new DInteger(v1 * v2);
	}

	private static DFraction mul (DFraction f , DInteger i) {
		long numerator = f.getNumerator() * i.getRep();
		long denominator = f.getDenominator();

		return new DFraction(numerator , denominator);
	}

	private static DFraction mul (DFraction fa , DFraction fb) {
		long numerator = fa.getNumerator() * fb.getNumerator();
		long denominator = fa.getDenominator() * fb.getDenominator();

		return new DFraction(numerator , denominator);
	}
	
	private static DFraction div (DInteger i1 , DInteger i2) {
		long numerator = i1.getRep();
		long denominator = i2.getRep();

		return new DFraction(numerator , denominator);
	}

	private static DFraction div (DFraction f , DInteger i) {
		return DNumber.mul(f , new DFraction(1 , i.getRep()));
	}

	private static DFraction div (DFraction fa , DFraction fb) {
		return DNumber.mul(fa , DFraction.inverse(fb));
	}
	
	
	abstract DNumber negate ();

}

class DInteger extends DNumber {

	private final long val;

	DInteger (long val) {
		this.val = val;
	}

	long getRep () {
		return val;
	}

	@Override
	public String toString () {
		return Long.toString(val);
	}
	
	@Override
	DNumber negate () {
		return new DInteger(-val);
	}

}

class DFraction extends DNumber {

	private static long GCD (long x , long y) {
		x = Math.abs(x);
		y = Math.abs(y);

		while (x != y && x > 0 && y > 0) {
			if (x > y) {
				x -= y;
			}
			else {
				y -= x;
			}
		}

		return Math.max(x , y);
	}
	
	static DFraction inverse (DFraction f) {
		long numerator = f.getDenominator();
		long denominator = f.getNumerator();
		
		return new DFraction(numerator , denominator);
	}

	
	private final long numerator;
	private final long denominator;

	DFraction (long numerator , long denominator) {
		if (denominator == 0) {
			throw new DError("Division by zero");
		}
		else {
			long gcd = GCD(numerator , denominator);

			this.numerator = numerator / gcd;
			this.denominator = denominator / gcd;
		}
	}

	DFraction (DNumber numerator , DNumber denominator) {
		DFraction f = (DFraction)DNumber.div(numerator , denominator);

		this.numerator = f.getNumerator();
		this.denominator = f.getDenominator();
	}

	long getNumerator () {
		return numerator;
	}

	long getDenominator () {
		return denominator;
	}

	@Override
	public String toString () {		
		if (denominator == 1) {
			return Long.toString(numerator);
		}
		else {
			return String.format("%d/%d" , numerator , denominator);
		}
	}

	double toFloat () {
		return ((double)numerator) / ((double)denominator);
	}
	
	@Override
	DNumber negate () {
		return new DFraction(-numerator , denominator);
	}

}

class DLambda extends Dvalue {

	private final List<String> formals;
	private final AstExpr body;
	private final DynamicEnvPtr env_ptr;

	DLambda (List<String> formals , AstExpr body , DynamicEnvPtr env_ptr) {
		this.formals = new ArrayList<String>(formals);
		this.body = body;
		this.env_ptr = env_ptr;
	}

	DLambda (List<String> formals , AstExpr body , DynamicEnv env) {
		this.formals = new ArrayList<String>(formals);
		this.body = body;
		this.env_ptr = new DynamicEnvPtr(env);
	}

	List<String> getFormals () {
		return formals;
	}

	AstExpr getBody () {
		return body;
	}

	DynamicEnv getEnv () {
		return env_ptr.getEnv();
	}

	@Override
	public String toString () {
		return "#<procedure>";
	}

}