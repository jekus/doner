package doner.Doner.src;

import java.util.LinkedList;
import java.util.HashMap;


// Used for Letrec
class DynamicEnvPtr {

	private DynamicEnv env;

	DynamicEnvPtr () {
		env = null;
	}

	DynamicEnvPtr (DynamicEnv env) {
		this.env = env.clone();
	}

	DynamicEnv getEnv () {
		return env;
	}

	void setEnv (DynamicEnv env) {
		this.env = env.clone();
	}

}

// Assume the data structure is immutable
class DynamicEnv {

	private final LinkedList<HashMap<String , DRef>> contexts;

	DynamicEnv () {
		contexts = new LinkedList<HashMap<String , DRef>>();
	}

	@SuppressWarnings("unchecked")
	private DynamicEnv (LinkedList<HashMap<String , DRef>> old_contexts) {
		contexts = new LinkedList<HashMap<String , DRef>>();
		for (HashMap<String , DRef> ctx : old_contexts) {
			contexts.addLast((HashMap<String , DRef>)ctx.clone());
		}
	}

	@Override
	protected DynamicEnv clone () {
		return new DynamicEnv(contexts);
	}

	DRef getVarInContexts (String varname) {
		for (HashMap<String , DRef> ctx : contexts) {
			DRef ref = ctx.get(varname);
			if (ref != null) {
				return ref;
			}
		}

		return null;
	}

	DRef getVar (String varname) {
		DRef ref = getVarInContexts(varname);
		if (ref == null) {
			throw new DError(String.format("Undefined variable : '%s'" , varname));
		}
		else {
			return ref;
		}
	}

	void pushVar (String varname , DRef ref) {
		contexts.getFirst().put(varname , ref);
	}

	void enterNewContext () {
		contexts.addFirst(new HashMap<String , DRef>());
	}
	
	void exitCurrentContext () {
		contexts.removeFirst();
	}

}

// Assume the data structure is immutable
class StaticEnv {

	private final LinkedList<HashMap<String , DType>> contexts;

	StaticEnv () {
		contexts = new LinkedList<HashMap<String , DType>>();
	}
	
	@SuppressWarnings("unchecked")
	private StaticEnv (LinkedList<HashMap<String , DType>> old_contexts) {
		contexts = new LinkedList<HashMap<String , DType>>();
		for (HashMap<String , DType> ctx : old_contexts) {
			contexts.addLast((HashMap<String , DType>)ctx.clone());
		}
	}

	@Override
	protected StaticEnv clone () {
		return new StaticEnv(contexts);
	}

	DType getVarInContexts (String varname) {
		for (HashMap<String , DType> ctx : contexts) {
			DType type = ctx.get(varname);
			if (type != null) {
				return type;
			}
		}

		return null;
	}

	DType getVar (String varname) {
		DType type = getVarInContexts(varname);
		if (type == null) {
			throw new DError(String.format("Undefined variable : '%s'" , varname));
		}
		else {
			return type;
		}
	}

	void pushVar (String varname , DType type) {
		contexts.getFirst().put(varname , type);
	}

	void addNewContext () {
		contexts.addFirst(new HashMap<String , DType>());
	}
	
	void exitCurrentContext () {
		contexts.removeFirst();
	}

}