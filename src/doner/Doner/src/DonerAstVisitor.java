package doner.Doner.src;


abstract class DonerAstVisitor<T> {

	protected final T visit (DonerAST ast) {
		if (ast instanceof AstProg) {
			return visitAstProg((AstProg)ast);
		}
		else if (ast instanceof AstLetExpr) {
			return visitAstLetExpr((AstLetExpr)ast);
		}
		else if (ast instanceof AstLetSeqExpr) {
			return visitAstLetSeqExpr((AstLetSeqExpr)ast);
		}
		else if (ast instanceof AstLetrecExpr) {
			return visitAstLetrecExpr((AstLetrecExpr)ast);
		}
		else if (ast instanceof AstLambdaExpr) {
			return visitAstLambdaExpr((AstLambdaExpr)ast);
		}
		else if (ast instanceof AstApplyExpr) {
			return visitAstApplyExpr((AstApplyExpr)ast);
		}
		else if (ast instanceof AstVarExpr) {
			return visitAstVarExpr((AstVarExpr)ast);
		}
		else if (ast instanceof AstIntegerExpr) {
			return visitAstIntegerExpr((AstIntegerExpr)ast);
		}
		else if (ast instanceof AstAndExpr) {
			return visitAstAndExpr((AstAndExpr)ast);
		}
		else if (ast instanceof AstOrExpr) {
			return visitAstOrExpr((AstOrExpr)ast);
		}
		else {
			return null;
		}
	}

	protected abstract T visitAstProg (AstProg ast);

	protected abstract T visitAstLetExpr (AstLetExpr ast);

	protected abstract T visitAstLetSeqExpr (AstLetSeqExpr ast);

	protected abstract T visitAstLetrecExpr (AstLetrecExpr ast);

	protected abstract T visitAstLambdaExpr (AstLambdaExpr ast);

	protected abstract T visitAstApplyExpr (AstApplyExpr ast);

	protected abstract T visitAstVarExpr (AstVarExpr ast);

	protected abstract T visitAstIntegerExpr (AstIntegerExpr ast);

	protected abstract T visitAstAndExpr (AstAndExpr ast);

	protected abstract T visitAstOrExpr (AstOrExpr ast);

}
