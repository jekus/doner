package doner.Doner.src;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;


class DonerTest {

	private static final String testsuit_root = "/home/jekus/Workspace/Java/Doner/test/";

	private static Dvalue testInterp (String filename) throws IOException {
		return DonerInterp.interp(testsuit_root + filename);
	}

	private static DType testTypeEngine (String filename) throws IOException {
		InputStream is = new FileInputStream(testsuit_root + filename);
		ANTLRInputStream input = new ANTLRInputStream(is);
		DonerLexer lexer = new DonerLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		DonerParser parser = new DonerParser(tokens);
		ParseTree tree = parser.prog();
		DonerParseTree2AST conv = new DonerParseTree2AST();
		DonerAST ast = conv.visit(tree);
		DTypeEngine te = new DTypeEngine();
		
		return te.run(ast);
	}

	@Test
	void testInteger () throws IOException {
		assertEquals(new IntegerType() , testTypeEngine("integer_test.dr"));
		assertEquals(1 , ((DInteger)testInterp("integer_test.dr")).getRep());
	}
	
	@Test
	void testDefine () throws IOException {
		assertEquals(new IntegerType() , testTypeEngine("define_test_1.dr"));
		assertEquals(1 , ((DInteger)testInterp("define_test_1.dr")).getRep());
	}

	@Test
	void testLet () throws IOException {
		assertEquals(new IntegerType() , testTypeEngine("let_test_1.dr"));
		assertThrows(DError.class , () -> {testTypeEngine("let_test_2.dr");});
		assertEquals(new IntegerType() , testTypeEngine("let_test_3.dr"));
		assertEquals(new IntegerType() , testTypeEngine("let_test_4.dr"));
		
		assertEquals(3 , ((DInteger)testInterp("let_test_1.dr")).getRep());
		assertThrows(DError.class , () -> {testInterp("let_test_2.dr");});
		assertEquals(5 , ((DInteger)testInterp("let_test_3.dr")).getRep());
		assertEquals(14 , ((DInteger)testInterp("let_test_4.dr")).getRep());
	}

	@Test
	void testLetSeq () throws IOException {
		assertEquals(new IntegerType() , testTypeEngine("let_seq_test_1.dr"));
		assertEquals(new IntegerType() , testTypeEngine("let_seq_test_2.dr"));
		
		assertEquals(5		,	((DInteger)testInterp("let_seq_test_1.dr")).getRep());
		assertEquals(1024	,	((DInteger)testInterp("let_seq_test_2.dr")).getRep());
	}

	@Test
	void testLetrec () throws IOException {
		assertEquals(new IntegerType() , testTypeEngine("letrec_test_1.dr"));
		assertEquals(new IntegerType() , testTypeEngine("letrec_test_2.dr"));
		assertEquals(new IntegerType() , testTypeEngine("letrec_test_3.dr"));
		assertEquals(new IntegerType() , testTypeEngine("letrec_test_4.dr"));
		assertEquals(new IntegerType() , testTypeEngine("letrec_test_5.dr"));
		assertEquals(new IntegerType() , testTypeEngine("letrec_test_6.dr"));
		
		assertEquals(1		,	((DInteger)testInterp("letrec_test_1.dr")).getRep());
		assertEquals(0		,	((DInteger)testInterp("letrec_test_2.dr")).getRep());
		assertEquals(6765	,	((DInteger)testInterp("letrec_test_3.dr")).getRep());
		assertEquals(5		,	((DInteger)testInterp("letrec_test_4.dr")).getRep());
		assertEquals(1024	,	((DInteger)testInterp("letrec_test_5.dr")).getRep());
		assertEquals(124	,	((DInteger)testInterp("letrec_test_6.dr")).getRep());
	}

	@Test
	void testEqual () throws IOException {
		assertEquals(new BooleanType() , testTypeEngine("equal_test_1.dr"));
		assertEquals(new BooleanType() , testTypeEngine("equal_test_2.dr"));
		
		assertFalse(((DBoolean)testInterp("equal_test_1.dr")).getRep());
		assertTrue(((DBoolean)testInterp("equal_test_2.dr")).getRep());
	}

	@Test
	void testGreater () throws IOException {
		assertEquals(new BooleanType() , testTypeEngine("greater_test_1.dr"));
		assertEquals(new BooleanType() , testTypeEngine("greater_test_2.dr"));
		
		assertFalse(((DBoolean)testInterp("greater_test_1.dr")).getRep());
		assertTrue(((DBoolean)testInterp("greater_test_2.dr")).getRep());
	}

	@Test
	void testLess () throws IOException {
		assertEquals(new BooleanType() , testTypeEngine("less_test_1.dr"));
		assertEquals(new BooleanType() , testTypeEngine("less_test_2.dr"));
		
		assertFalse(((DBoolean)testInterp("less_test_1.dr")).getRep());
		assertTrue(((DBoolean)testInterp("less_test_2.dr")).getRep());
	}

	@Test
	void testGreaterEqual () throws IOException {
		assertEquals(new BooleanType() , testTypeEngine("greater_equal_test_1.dr"));
		assertEquals(new BooleanType() , testTypeEngine("greater_equal_test_2.dr"));
		
		assertFalse(((DBoolean)testInterp("greater_equal_test_1.dr")).getRep());
		assertTrue(((DBoolean)testInterp("greater_equal_test_2.dr")).getRep());
	}

	@Test
	void testLessEqual () throws IOException {
		assertEquals(new BooleanType() , testTypeEngine("less_equal_test_1.dr"));
		assertEquals(new BooleanType() , testTypeEngine("less_equal_test_2.dr"));
		
		assertFalse(((DBoolean)testInterp("less_equal_test_1.dr")).getRep());
		assertTrue(((DBoolean)testInterp("less_equal_test_2.dr")).getRep());
	}

	@Test
	void testIf () throws IOException {
		assertEquals(new IntegerType() , testTypeEngine("if_test_1.dr"));
		assertEquals(new IntegerType() , testTypeEngine("if_test_2.dr"));
		
		assertEquals(1 , ((DInteger)testInterp("if_test_1.dr")).getRep());
		assertEquals(2 , ((DInteger)testInterp("if_test_2.dr")).getRep());
	}

	@Test
	void testAnd () throws IOException {
		assertEquals(new BooleanType() , testTypeEngine("and_test_1.dr"));
		assertEquals(new BooleanType() , testTypeEngine("and_test_2.dr"));
		
		assertTrue(((DBoolean)testInterp("and_test_1.dr")).getRep());
		assertFalse(((DBoolean)testInterp("and_test_2.dr")).getRep());
	}

	@Test
	void testOr () throws IOException {
		assertEquals(new BooleanType() , testTypeEngine("or_test_1.dr"));
		assertEquals(new BooleanType() , testTypeEngine("or_test_2.dr"));
		
		assertTrue(((DBoolean)testInterp("or_test_1.dr")).getRep());
		assertFalse(((DBoolean)testInterp("or_test_2.dr")).getRep());
	}

	@Test
	void testNot () throws IOException {
		assertEquals(new BooleanType() , testTypeEngine("not_test.dr"));
		assertFalse(((DBoolean)testInterp("not_test.dr")).getRep());
	}

}
