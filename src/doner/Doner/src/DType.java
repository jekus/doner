package doner.Doner.src;

import java.util.List;
import java.util.Iterator;
import java.util.LinkedList;


abstract class DType extends DonerAST {

	abstract DType applySubstitution (DSubstitution subst);

	abstract DType subsTvar (TypeVar tvar , DType t);

	abstract boolean haveTvar (TypeVar tvar);

	Dvalue interp (DynamicEnv env , DStore store) {
		return null;
	}

	DType getType (StaticEnv senv , DSubstitution subst) {
		return null;
	}

}

abstract class PrimitiveProcType extends DType {

	@Override
	DType applySubstitution (DSubstitution subst) {
		return this;
	}

	@Override
	DType subsTvar (TypeVar tvar , DType t) {
		return this;
	}

	@Override
	boolean haveTvar (TypeVar tvar) {
		return false;
	}

	abstract void unifyWith (LambdaType lb_t , DSubstitution subst);

}

class PrimitiveAddType extends PrimitiveProcType {

	@Override
	void unifyWith (LambdaType lb_t , DSubstitution subst) {
		DType int_t = new IntegerType();

		for (DType t : lb_t.getFormalsType()) {
			subst.unify(int_t , t);
		}

		subst.unify(int_t , lb_t.getResultType());
	}

}

class PrimitiveSubType extends PrimitiveProcType {

	@Override
	void unifyWith (LambdaType lb_t , DSubstitution subst) {
		List<DType> actual_formals_types = lb_t.getFormalsType();

		if (actual_formals_types.size() < 1) {
			throw new DError(String.format("procedure '-' expects at least 1 argument"));
		}
		else {
			DType int_t = new IntegerType();

			for (DType t : actual_formals_types) {
				subst.unify(int_t , t);
			}

			subst.unify(int_t , lb_t.getResultType());
		}
	}

}

class PrimitiveMulType extends PrimitiveProcType {

	@Override
	void unifyWith (LambdaType lb_t , DSubstitution subst) {
		DType int_t = new IntegerType();

		for (DType t : lb_t.getFormalsType()) {
			subst.unify(int_t , t);
		}

		subst.unify(int_t , lb_t.getResultType());
	}

}

class PrimitiveDivType extends PrimitiveProcType {

	@Override
	void unifyWith (LambdaType lb_t , DSubstitution subst) {
		List<DType> actual_formals_types = lb_t.getFormalsType();

		if (actual_formals_types.size() < 1) {
			throw new DError(String.format("procedure '/' expects at least 1 argument"));
		}
		else {
			DType int_t = new IntegerType();

			for (DType t : actual_formals_types) {
				subst.unify(int_t , t);
			}

			subst.unify(int_t , lb_t.getResultType());
		}
	}

}

class PrimitiveEqualType extends PrimitiveProcType {

	@Override
	void unifyWith (LambdaType lb_t , DSubstitution subst) {
		DType int_t = new IntegerType();

		for (DType t : lb_t.getFormalsType()) {
			subst.unify(int_t , t);
		}

		subst.unify(new BooleanType() , lb_t.getResultType());
	}

}

class PrimitiveGreaterType extends PrimitiveProcType {

	@Override
	void unifyWith (LambdaType lb_t , DSubstitution subst) {
		DType int_t = new IntegerType();

		for (DType t : lb_t.getFormalsType()) {
			subst.unify(int_t , t);
		}

		subst.unify(new BooleanType() , lb_t.getResultType());
	}

}

class PrimitiveLessThanType extends PrimitiveProcType {

	@Override
	void unifyWith (LambdaType lb_t , DSubstitution subst) {
		DType int_t = new IntegerType();

		for (DType t : lb_t.getFormalsType()) {
			subst.unify(int_t , t);
		}

		subst.unify(new BooleanType() , lb_t.getResultType());
	}

}

class PrimitiveGreaterEqualType extends PrimitiveProcType {

	@Override
	void unifyWith (LambdaType lb_t , DSubstitution subst) {
		DType int_t = new IntegerType();

		for (DType t : lb_t.getFormalsType()) {
			subst.unify(int_t , t);
		}

		subst.unify(new BooleanType() , lb_t.getResultType());
	}

}

class PrimitiveLessEqualType extends PrimitiveProcType {

	@Override
	void unifyWith (LambdaType lb_t , DSubstitution subst) {
		DType int_t = new IntegerType();

		for (DType t : lb_t.getFormalsType()) {
			subst.unify(int_t , t);
		}

		subst.unify(new BooleanType() , lb_t.getResultType());
	}

}

class PrimitiveNotType extends PrimitiveProcType {

	@Override
	void unifyWith (LambdaType lb_t , DSubstitution subst) {
		BooleanType bool_t = new BooleanType();

		subst.unify(bool_t , lb_t.getFormalsType().get(0));
		subst.unify(bool_t , lb_t.getResultType());
	}

}

class VoidType extends DType {

	@Override
	DType applySubstitution (DSubstitution subst) {
		return this;
	}

	@Override
	DType subsTvar (TypeVar tvar , DType t) {
		return this;
	}

	@Override
	boolean haveTvar (TypeVar tvar) {
		return false;
	}

}

class TypeVar extends DType {

	private static int next_typeid = 0;

	private final int typeid;

	TypeVar (DSubstitution subst) {
		typeid = next_typeid;
		++next_typeid;
	}

	@Override
	DType applySubstitution (DSubstitution subst) {
		DType t = subst.get(this);

		while (t instanceof TypeVar && t != this) {
			t = subst.get((TypeVar)t);
		}

		return t;
	}

	@Override
	DType subsTvar (TypeVar tvar , DType t) {
		if (tvar.typeid == typeid) {
			return t;
		}
		else {
			return this;
		}
	}

	@Override
	boolean haveTvar (TypeVar tvar) {
		return tvar.typeid == typeid;
	}

	int getID () {
		return typeid;
	}

	@Override
	public String toString () {
		return String.format("<tvar : %d>" , typeid);
	}

	@Override
	public boolean equals (Object obj) {
		if (obj instanceof TypeVar) {
			return typeid == ((TypeVar)obj).typeid;
		}
		else {
			return false;
		}
	}

}

class DataType extends DType {

	private final String name;

	DataType (String name) {
		this.name = name;
	}

	String getName () {
		return name;
	}

	@Override
	DType applySubstitution (DSubstitution subst) {
		return this;
	}

	@Override
	DType subsTvar (TypeVar tvar , DType t) {
		return this;
	}

	@Override
	boolean haveTvar (TypeVar tvar) {
		return false;
	}

	@Override
	public String toString () {
		return String.format("<Datatype %s>" , name);
	}

	@Override
	public boolean equals (Object obj) {
		if (obj instanceof DataType) {
			return name.equals(((DataType)obj).name);
		}
		else {
			return false;
		}
	}

}

class BooleanType extends DType {

	@Override
	DType applySubstitution (DSubstitution subst) {
		return this;
	}

	@Override
	DType subsTvar (TypeVar tvar , DType t) {
		return this;
	}

	@Override
	boolean haveTvar (TypeVar tvar) {
		return false;
	}

	@Override
	public String toString () {
		return "Boolean";
	}

	@Override
	public boolean equals (Object obj) {
		return (obj instanceof BooleanType);
	}

}

abstract class NumberType extends DType {
	
}

class IntegerType extends NumberType {

	@Override
	DType applySubstitution (DSubstitution subst) {
		return this;
	}

	@Override
	DType subsTvar (TypeVar tvar , DType t) {
		return this;
	}

	@Override
	boolean haveTvar (TypeVar tvar) {
		return false;
	}

	@Override
	public String toString () {
		return "Integer";
	}

	@Override
	public boolean equals (Object obj) {
		return (obj instanceof IntegerType);
	}

}

class FractionType extends NumberType {
	
	@Override
	DType applySubstitution (DSubstitution subst) {
		return this;
	}

	@Override
	DType subsTvar (TypeVar tvar , DType t) {
		return this;
	}

	@Override
	boolean haveTvar (TypeVar tvar) {
		return false;
	}

	@Override
	public String toString () {
		return "Fraction";
	}

	@Override
	public boolean equals (Object obj) {
		return (obj instanceof FractionType);
	}
	
}

class LambdaType extends DType {

	private final List<DType> formals_type;
	private final DType res_type;

	LambdaType (List<DType> ft , DType rt) {
		formals_type = ft;
		res_type = rt;
	}

	List<DType> getFormalsType () {
		return formals_type;
	}

	DType getResultType () {
		return res_type;
	}

	@Override
	DType applySubstitution (DSubstitution subst) {
		List<DType> new_formals_type = new LinkedList<DType>();
		for (DType t : formals_type) {
			new_formals_type.add(subst.apply(t));
		}

		return new LambdaType(new_formals_type , subst.apply(res_type));
	}

	@Override
	DType subsTvar (TypeVar tvar , DType t) {
		List<DType> new_formals_type = new LinkedList<DType>();

		for (DType type : formals_type) {
			new_formals_type.add(type.subsTvar(tvar , t));
		}

		return new LambdaType(new_formals_type , res_type.subsTvar(tvar , t));
	}

	@Override
	boolean haveTvar (TypeVar tvar) {
		for (DType type : formals_type) {
			if (type.haveTvar(tvar)) {
				return true;
			}
		}

		return res_type.haveTvar(tvar);
	}

	@Override
	public String toString () {
		StringBuilder sb = new StringBuilder();
		sb.append("(->");

		for (DType t : formals_type) {
			sb.append(' ');
			sb.append(t.toString());
		}

		sb.append(' ');
		sb.append(res_type.toString());
		sb.append(')');

		return sb.toString();
	}

	@Override
	public boolean equals (Object obj) {
		if (obj instanceof LambdaType) {
			LambdaType lt = (LambdaType)obj;
			Iterator<DType> iter_1 = formals_type.iterator();
			Iterator<DType> iter_2 = lt.formals_type.iterator();

			if (formals_type.size() != lt.formals_type.size()) {
				return false;
			}

			while (iter_1.hasNext()) {
				if (!iter_1.next().equals(iter_2.next())) {
					return false;
				}
			}

			return res_type.equals(lt.res_type);
		}
		else {
			return false;
		}
	}

}