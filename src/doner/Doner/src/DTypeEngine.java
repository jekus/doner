package doner.Doner.src;


// Do the type checking and inference
class DTypeEngine {

	private final StaticEnv senv;
	private final DSubstitution subst;

	DTypeEngine () {
		senv = new StaticEnv();
		subst = new DSubstitution();

		senv.addNewContext();
		senv.pushVar("+" , new PrimitiveAddType());
		senv.pushVar("-" , new PrimitiveSubType());
		senv.pushVar("*" , new PrimitiveMulType());
		senv.pushVar("/" , new PrimitiveDivType());
		senv.pushVar("=" , new PrimitiveEqualType());
		senv.pushVar(">" , new PrimitiveGreaterType());
		senv.pushVar("<" , new PrimitiveLessThanType());
		senv.pushVar(">=" , new PrimitiveGreaterEqualType());
		senv.pushVar("<=" , new PrimitiveLessEqualType());
		senv.pushVar("not" , new PrimitiveNotType());
	}

	DType run (DonerAST ast) {
		return subst.apply(ast.getType(senv , subst));
	}

}